﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;

namespace ServiceMonitorTool306
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 30000; // 30 seconds
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();



        }

        public class sendmails
        { //Only send 1 failure email
            public static int[] SendEmail = new int[6] { 1, 1, 1, 1, 1, 1 };
        }
        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            // TODO: Insert monitoring activities here.
            string[] ServiceArray = new string[6] { "", "", "", "", "", "" };
            //string Subject = "Windows Service Failure - WEGDAAP308";
            string Subject = "Windows Service Failure - WEGDAAP306";

            bool TaskSchedule;
            bool DSNetSOAP;
            bool DSNetSock;
            bool Powerplay;
            bool Forecasting;
            bool amserver;
            System.ServiceProcess.ServiceController sc = new System.ServiceProcess.ServiceController("Schedule");
            System.ServiceProcess.ServiceController sd = new System.ServiceProcess.ServiceController("Cognos DecisionStream Network Services SOAP Server (Apache Tomcat) (cer3)");
            System.ServiceProcess.ServiceController se = new System.ServiceProcess.ServiceController("Cognos DecisionStream Network Services Socket Server (cer3)");
            System.ServiceProcess.ServiceController sf = new System.ServiceProcess.ServiceController("Cognos PowerPlay Enterprise Server (cer4)");
            System.ServiceProcess.ServiceController sg = new System.ServiceProcess.ServiceController("Forecasting Services (On Trade)");
            System.ServiceProcess.ServiceController sh = new System.ServiceProcess.ServiceController("amserver_cer4");
            // Stops processing between 6:59pm and 7:05pm as there is a job which restarts the cognos services.
            TimeSpan start = new TimeSpan(18, 59, 0); //6:59pm
            TimeSpan end = new TimeSpan(19, 05, 0); //7:05pm
            TimeSpan now = DateTime.Now.TimeOfDay;
            TimeSpan trimmedTimeNow = new TimeSpan(now.Hours, now.Minutes, now.Seconds);


            // if between 6:59 and 7:05 then do nothing else preform checks.
            if ((trimmedTimeNow > start && trimmedTimeNow < end) && DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
            { }
            else
            {
                //If to Check Task scheduler
                if (sc.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                {
                    TaskSchedule = true;
                    ServiceArray[0] = "<tr><td><span style=font-weight:bold>Task Scheduler</td><td>Stopped</td></tr>";
                    //sc.Start();
                }
                else
                {
                    TaskSchedule = false;
                    sendmails.SendEmail[0] = 1;
                }
                try { }
                catch (Exception ex)
                {
                    return;
                }


                //IF to check DS Network SOAP
                if (sd.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                {
                    DSNetSOAP = true;
                    //sd.Start();
                    ServiceArray[1] = "<tr><td><span style=font-weight:bold>DS Network SOAP</td><td>Stopped</td></tr>";
                }
                else
                {
                    DSNetSOAP = false;
                    sendmails.SendEmail[1] = 1;
                }
                try { }
                catch (Exception ex)
                {
                    return;
                }

                //If to Check DS Network Socket

                if (se.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                {
                    DSNetSock = true;
                    //se.Start();
                    ServiceArray[2] = "<td><span style=font-weight:bold>DS Network Socket</td><td>Stopped</td></tr>";
                }
                else
                {
                    DSNetSock = false;
                    sendmails.SendEmail[2] = 1;
                }
                try { }
                catch (Exception ex)
                {
                    return;
                }

                //IF to Check PowerPlay

                if (sf.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                {
                    Powerplay = true;
                    //sf.Start();
                    ServiceArray[3] = "<tr><td><span style=font-weight:bold>PowerPlay</td><td>Stopped</td></tr>";
                }
                else
                {
                    Powerplay = false;
                    sendmails.SendEmail[3] = 1;
                }
                try { }
                catch (Exception ex)
                {
                    return;
                }

                //If to Check Forecasting

                if (sg.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                {
                    Forecasting = true;
                    // sg.Start();
                    ServiceArray[4] = "<tr><td><span style=font-weight:bold>Forecasting</td><td>Stopped</td></tr>";
                }
                else
                {
                    Forecasting = false;
                    sendmails.SendEmail[4] = 1;
                }
                try { }
                catch (Exception ex)
                {
                    return;
                }

                // IF to check Access Manager

                if (sh.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
                {
                    amserver = true;
                    //sh.Start();
                    ServiceArray[5] = "<tr><td><span style=font-weight:bold>Access Manager</td><td>Stopped</td></tr>";
                }
                else
                {
                    amserver = false;
                    sendmails.SendEmail[5] = 1;
                }
                try { }
                catch (Exception ex)
                {
                    return;
                }


                string Body = "Dear Team, <p> This email has been sent as there have been the following service failures: <p> <table border =1>" + ServiceArray[0] + ServiceArray[1] + ServiceArray[2] + ServiceArray[3] + ServiceArray[4] + ServiceArray[5] + "</table> <p> Regards<p> EBI Solutions";

                // If to check for true coditions and to send email alert || means or

                if (TaskSchedule == true || DSNetSOAP == true || DSNetSock == true || Powerplay == true || Forecasting == true || amserver == true)
                {
                    if (sendmails.SendEmail[0] == 1 && sendmails.SendEmail[1] == 1 && sendmails.SendEmail[2] == 1 && sendmails.SendEmail[3] == 1 && sendmails.SendEmail[4] == 1 && sendmails.SendEmail[2] == 1)
                    {
                        try
                        {
                            MailMessage mail = new MailMessage();
                            SmtpClient SmtpServer = new SmtpClient("SMTP-RELAY.WE.INTERBREW.NET");

                            mail.From = new MailAddress("ds_production@inbev.co.uk");
                            mail.To.Add("production@ebisolutions.co.uk");
                            //mail.To.Add("aaron.gleeson@ebisolutions.co.uk");
                            mail.Subject = Subject;
                            mail.Body = Body;
                            mail.IsBodyHtml = true;

                            SmtpServer.Port = 25;
                            SmtpServer.Send(mail);


                            //only send 1 failure email
                            sendmails.SendEmail[0] = 0;
                            sendmails.SendEmail[1] = 0;
                            sendmails.SendEmail[2] = 0;
                            sendmails.SendEmail[3] = 0;
                            sendmails.SendEmail[4] = 0;
                            sendmails.SendEmail[5] = 0;


                        }

                        catch (Exception ex)
                        {

                        }
                    }
                }

            }
        }


        protected override void OnStop()
        {
        }


    }
}