﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;


namespace ServiceMonitorTool305
{
    public partial class MonitoringTool : ServiceBase
    {
        public MonitoringTool()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 30000; // 30 seconds
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();
                        
        }

        public class sendmails

        { //Only send 1 failure email
            public static int[] SendEmail = new int[3] { 1, 1, 1 };
        }

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            // Insert monitoring activities here.
            string[] ServiceArray = new string[5] { "", "", "", "", "" };
            bool TaskSchedule;
            bool DSNetSOAP;
            bool DSNetSock;
            string Subject = "Windows Service Failure - WEGDAAP307";
            //string Subject = "Windows Service Failure - WEGDAAP305";
           // string Subject = "Windows Service Failure - WEGDAAP309"; 
            System.ServiceProcess.ServiceController sc = new System.ServiceProcess.ServiceController("Schedule");
            System.ServiceProcess.ServiceController sd = new System.ServiceProcess.ServiceController("Cognos DecisionStream Network Services SOAP Server (Apache Tomcat) (cer3)");
            System.ServiceProcess.ServiceController se = new System.ServiceProcess.ServiceController("Cognos DecisionStream Network Services Socket Server (cer3)");

            if (sc.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
            {
                TaskSchedule = true;
                ServiceArray[0] = "<tr><td><span style=font-weight:bold>Task Scheduler</span></td><td>Stopped</td></tr>";
                //sc.Start();
            }
            else
            { TaskSchedule = false;
              sendmails.SendEmail[0] = 1;
            }
            try { }
            catch (Exception ex)
            {
                return;
            }

            if (sd.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
            {
                DSNetSOAP = true;
                //sd.Start();
                ServiceArray[1] = "<tr><td><span style=font-weight:bold>DS Network SOAP</span></td><td>Stopped</td></tr>";
            }
            else
            {
                DSNetSOAP = false;
                sendmails.SendEmail[1] = 1;
            }
            try { }
            catch (Exception ex)
            {
                return;
            }
            if (se.Status == System.ServiceProcess.ServiceControllerStatus.Stopped)
            {
                DSNetSock = true;
                //se.Start();
                ServiceArray[2] = "<tr><td><span style=font-weight:bold>DS Network Socket</span></td><td>Stopped</td></tr>";
            }
            else
            {
                DSNetSock = false;
                sendmails.SendEmail[2] = 1;
            }
            try { }
            catch (Exception ex)
            {
                return;
            }
            string Body = string.Concat("Dear Team, <p> This email has been sent as there have been the following service failures: <p> <table border =1>", ServiceArray[0],  ServiceArray[1], ServiceArray[2], " </table>  <p> Regards<p> EBI Solutions");

            if (TaskSchedule == true || DSNetSOAP == true || DSNetSock == true)
            {
                if (sendmails.SendEmail[0] == 1 && sendmails.SendEmail[1] == 1 && sendmails.SendEmail[2] == 1)
                {
                    try
                    {
                        MailMessage mail = new MailMessage();
                        SmtpClient SmtpServer = new SmtpClient("SMTP-RELAY.WE.INTERBREW.NET");

                        mail.From = new MailAddress("ds_production@inbev.co.uk");
                        mail.To.Add("production@ebisolutions.co.uk");
                        mail.Subject = Subject;
                        mail.Body = Body;
                        mail.IsBodyHtml = true;

                        SmtpServer.Port = 25;
                        SmtpServer.Send(mail);


                        //Only send 1 failure email
                        sendmails.SendEmail[0] = 0;
                        sendmails.SendEmail[1] = 0;
                        sendmails.SendEmail[2] = 0;
                    }

                    catch (Exception ex)
                    {

                    }
                }
            }
        }



        protected override void OnStop()
        {
        }


    }
}
